let () =
    let handle_htmx ~req body =
        match Dream.header req "HX-Request" with
        | None   -> Dream_html.respond @@ Templates.page ~title:"leyline" body
        | Some _ -> Dream_html.respond @@ body in
    Dream.run ~port:8080 ~interface:"0.0.0.0"
    @@ Dream.logger 
    @@ Dream.memory_sessions
    @@ Dream.router [

        (* STATIC FILES *)
         Dream.get "/static/**" 
            @@ Dream.static 
            ~loader:(fun _root path _req -> 
                match G_crunch.G_styles.read path with 
                | None -> Dream.empty `Not_Found 
                | Some asset -> Dream.respond asset)
            "";

        (* HOMEPAGE *)
        Dream.get "/" (fun req -> 
            let open Dream_html in
            let open HTML in
            handle_htmx ~req @@ main [] [ h1 [] [txt "leyline"]]);
    ];;
